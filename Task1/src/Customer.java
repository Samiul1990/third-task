public class Customer {
    private int customerId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;

    Customer(int customerId, String customerName, String customerAddress, String customerPhone)
    {
        this.customerId = customerId;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.customerPhone = customerPhone;
    }

    public int getCustomerId()
    {
        return customerId;
    }
    public String getCustomerName()
    {
        return customerName;
    }
    public String getCustomerAddress()
    {
        return customerAddress;
    }
    public String getCustomerPhone()
    {
        return customerPhone;
    }


    public void setCustomerId(int customerId)
    {
        this.customerId = customerId;
    }
    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }
    public void setCustomerAddress(String customerAddress)
    {
        this.customerAddress = customerAddress;
    }
    public void setCustomerPhone(String customerPhone)
    {
        this.customerPhone = customerPhone;
    }


    public String toString() {
        return "Customer ID: " + customerId + "\n" + "Customer Name: " + customerName + "\n" +
                "Customer Address: " + customerAddress + "\n" +
                "Customer Phone: " + customerPhone + "\n";
    }
//    public String toString() {
//        return customerId + "";
//    }




}
