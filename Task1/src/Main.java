import java.util.ArrayList;

public class Main {
    static ArrayList<Product> prod = new ArrayList<Product>();
    static ArrayList<Product> prod2 = new ArrayList<Product>();
    static ArrayList<Customer> cus = new ArrayList<Customer>();
    static ArrayList<Invoice> inv = new ArrayList<Invoice>();

    public static void main(String[] args) {

        prod.add(new Product(1, "ProBook 440", 45000.00));
        prod.add(new Product(2, "Smart phone", 35000.00));
        prod.add(new Product(3, "Smart Watch ", 4500.00));

        prod2.add(new Product(1,"Tv", 50000.00));
        prod2.add(new Product(2,"AC", 85000.00));

        Invoice inv1 = new Invoice(1, new Customer(1, "Sami", "Dhaka", "0987654"), prod);
        Invoice inv2 = new Invoice(2, new Customer(2,"Sarker", "Gazipur", "098765"), prod2);

        inv.add(inv1);
        inv.add(inv2);

        for(int i = 0; i < inv.size(); i++)
        {
            System.out.println("Invoice ID: "+ inv.get(i).getInvoiceId());
            System.out.println("Customer details: "+ inv.get(i).getToString());

            for(int j = 0; j< inv.get(i).getProducts().size(); j++)
            {
                System.out.println("Products Id: "+inv.get(i).getProducts().get(j).getProductId());
                System.out.println("Products Name: "+inv.get(i).getProducts().get(j).getProductName());
                System.out.println("Products Price: "+inv.get(i).getProducts().get(j).getProductPrice());
            }
            System.out.println();

        }

    }
}
