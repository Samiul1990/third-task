import java.util.ArrayList;
import java.util.List;

public class Invoice {
    private int invoiceId;
    private ArrayList<Product> products;
    private Customer cust;

    Invoice(int invoiceId, Customer cust, ArrayList<Product> products)
    {
        this.invoiceId = invoiceId;
        this.cust= cust;
        this.products = products;
    }

    public int getInvoiceId()
    {
        return invoiceId;
    }


    public void setInvoiceId(int invoiceId)
    {
        this.invoiceId = invoiceId;
    }


    public ArrayList<Product>getProducts()
    {
        return products;
    }

    public void setProducts(ArrayList<Product> products)
    {
        this.products = products;
    }

    public String getToString()
    {
        return cust.toString();
    }

}